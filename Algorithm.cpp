// SPDX-License-Identifier: MIT
#include "Algorithm.h"

#include <vector>
#include <functional>

namespace AudioProcessor {
	using WindowFunction = Value(*)(size_t, size_t);

	class const_merged_vector {
		const Value* const _a;
		const Value* const _b;
		size_t _a_size, _b_size;
	public:
		const_merged_vector(const Value* const a, size_t a_size, const Value* const b, size_t b_size) :
			_a(a), _b(b), _a_size(a_size), _b_size(b_size) {}
		inline size_t size() const { return _a_size + _b_size; }
		inline const Value& operator[](size_t i) const {
			return i < _a_size ? _a[i] : _b[i - _a_size];
		}
	};

	class merged_vector {
		Value* const _a;
		Value* const _b;
		size_t _a_size, _b_size;
	public:
		merged_vector(Value* const a, size_t a_size, Value* const b, size_t b_size) :
			_a(a), _b(b), _a_size(a_size), _b_size(b_size) {}
		inline size_t size() const { return _a_size + _b_size; }
		inline Value& operator[](size_t i) {
			return i < _a_size ? _a[i] : _b[i - _a_size];
		}
	};

	template<class T>
	static std::tuple<size_t, Value> Autocorrelation(const T& buf, size_t size, size_t low, size_t high)
	{
		size_t ac = low;
		Value ac_value = 0;
		for (auto shift = low; shift < high; ++shift) {
			Value sum = 0;
			for (size_t i = 0; i < size - shift; ++i) {
				sum += buf[i] * buf[i + shift];
			}
			if (sum > ac_value) {
				ac = shift;
				ac_value = sum;
			}
		}
		return std::make_tuple(ac, ac_value);
	}

	static inline Value TriangleWindowFunction(size_t i, size_t width) {
		auto dx = Value((i < width / 2) ? i : width - i);
		return dx * 2 / width;
	}

	template<WindowFunction Func, class T>
	static inline void ClipWindow(const T& src, Value* dst, size_t shift, size_t src_width, size_t dst_width)
	{
		double delta_src = double(src_width) / double(dst_width);
		for (size_t i = 0; i < dst_width; ++i) {
			size_t src_i = size_t(delta_src * double(i));
			dst[i] = Func(src_i, src_width) * src[src_i + shift];
		}
	}

	Processor::Processor() :
		_pitch(1.0), _formant(1.0), _low(80), _high(500), _threshold(0.0002f),
		src_pos(0), dst_pos(0),
		buf(500 * 2, Value(0)), src_buf(500 * 2, Value(0)), dst_buf(500 * 2, Value(0)) {}

	void Processor::Process(const Value* const src, Value* const dst, size_t len)
	{
		// recover pre process
		std::copy(dst_buf.cbegin(), dst_buf.cend(), dst);
		std::fill(dst_buf.begin(), dst_buf.end(), Value(0));

		// process
		const_merged_vector merged_src(src_buf.data(), src_buf.size(), src, len);
		merged_vector merged_dst(dst, len, dst_buf.data(), dst_buf.size());
		size_t T;
		Value v;
		std::tie(T, v) = Autocorrelation(merged_src, len, _low, _high);
		if (v > _threshold) {
			size_t src_width = 2 * T, dst_width = size_t(float(2 * T) * _formant);
			size_t src_shift = T, dst_shift = size_t(float(T) * _pitch);
			buf.resize(dst_width);
			do {
				if (dst_pos >= src_pos && src_pos < len) {
					ClipWindow<TriangleWindowFunction>(merged_src, buf.data(), src_shift, src_width, dst_width);
					src_pos += src_shift;
				}
				if (dst_pos <= src_pos && dst_pos < len) {
					for (size_t i = 0; i < buf.size(); ++i) {
						merged_dst[dst_pos + i] += buf[i];
					}
					dst_pos += dst_shift;
				}
			} while (src_pos < len || dst_pos < len);
			src_pos -= len;
			dst_pos -= len;
		}
		else {
			src_pos = 0;
			dst_pos = 0;
		}

		// post process
		for (size_t i = 0; i < src_buf.size(); ++i) {
			src_buf[i] = merged_src[i + len];
		}
	}
}
