// SPDX-License-Identifier: MIT
#pragma once
#include<tuple>
#include<vector>

namespace AudioProcessor {
	using Value = float;

	class Processor {
	private:
		float _pitch;
		float _formant;
		size_t _low;
		size_t _high;
		Value _threshold;
		std::vector<Value> buf;
		std::vector<Value> src_buf;
		std::vector<Value> dst_buf;
		size_t src_pos, dst_pos;
	public:
		Processor();
		float pitch() const { return _pitch; }
		float pitch(float v) { return _pitch = v; }
		float formant() const { return _formant; }
		float formant(float v) { return _formant = v; }
		size_t low() const { return _low; }
		size_t low(size_t v) { return _low = v; }
		size_t high() const { return _high; }
		size_t high(size_t v) {
			buf.reserve(v * 2);
			src_buf.resize(v * 2, Value(0));
			dst_buf.resize(v * 2, Value(0));
			return _high = v;
		}
		Value threshold() const { return _threshold; }
		Value threshold(Value v) { return _threshold = v; }
		void Process(const Value* const src, Value* const dst, size_t len);
	};
}
